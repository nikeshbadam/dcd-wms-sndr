package com.ytm.dcd.wms.sender;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.retry.annotation.EnableRetry;

@EnableRetry
@SpringBootApplication
public class DcdWmsSenderApplication {

	public static void main(String[] args) {
		SpringApplication.run(DcdWmsSenderApplication.class, args);
	}

}
