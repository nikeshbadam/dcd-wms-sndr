package com.ytm.dcd.wms.sender.config.kafka;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

@Getter
@Setter
@Configuration
@EnableAutoConfiguration
@NoArgsConstructor
@ConfigurationProperties(prefix = "ytm.dcd.kafka")
public class KafkaProperties {
    public String bootstrapServers;
    public String schemaRegistryUrl;
    public String schemaRegistryAuth;
    public String schemaAuthSource;
    public String saslMechanism;
    public String saslJaasConfig;
    public String securityProtocol;
    public Map<String, Topic> topics;

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Topic {
        public String topicName;
        public String groupId;
        public Integer partitions;
        public Integer replicas;
        public String dlq;
    }
}
