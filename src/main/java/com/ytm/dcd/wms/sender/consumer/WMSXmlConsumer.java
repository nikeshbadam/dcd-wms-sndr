package com.ytm.dcd.wms.sender.consumer;

import com.ytm.dcd.wms.sender.service.WMSSenderService;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class WMSXmlConsumer {

    @Autowired
    private WMSSenderService senderService;

    @KafkaListener(topics = "#{'${ytm.dcd.kafka.topic-name}'}", containerFactory = "kafkaRetryListenerContainerFactory",
            groupId = "#{'${ytm.dcd.kafka.group-id}'}")
    public void processWmsXml(ConsumerRecord<String, String> consumerRecord, @Payload String xmlString) {
        log.info("Consuming records from topic: {} with event data: {}", consumerRecord.topic(), xmlString);
        senderService.sendXmlToWms(xmlString);
    }

}