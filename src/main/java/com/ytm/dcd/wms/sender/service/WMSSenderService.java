package com.ytm.dcd.wms.sender.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class WMSSenderService {

    @Autowired
    private JmsTemplate jmsTemplate;

    @Value("${ytm.dcd.wms.queue-name}")
    private String queueName;

    @Retryable(maxAttemptsExpression = "${ytm.dcd.wms.queue.retry.max-attempts}",
            value = Exception.class,
            backoff = @Backoff(delayExpression = "${ytm.dcd.wms.queue.retry.backoff-delay}")
    )
    public void sendXmlToWms(String message) {
        try {
            jmsTemplate.send(queueName, session -> session.createTextMessage(message));
        } catch (JmsException e) {
            log.error("Error occurred while sending data to the queue: {}, data: {}. Reason: {}", queueName, message,
                    e.getMessage());
        }
    }

}
