package com.ytm.dcd.wms.sender.service;

import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.test.util.ReflectionTestUtils;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;

@ExtendWith(MockitoExtension.class)
class WMSSenderServiceTest {

    @Mock
    private JmsTemplate jmsTemplate;

    @InjectMocks
    private WMSSenderService senderService;

    @BeforeEach
    void setUp() {
        ReflectionTestUtils.setField(senderService, "queueName", "otm-sndr");
    }

    @Test
    void sendXmlToWms() {
        MessageCreator xml = getXml();
        doNothing().when(jmsTemplate).send(anyString(), any());
        senderService.sendXmlToWms("xml");
    }

    @NotNull
    private MessageCreator getXml() {
        return session -> session.createTextMessage("xml");
    }
}