package com.ytm.dcd.wms.sender.consumer;

import com.ytm.dcd.wms.sender.service.WMSSenderService;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class WMSXmlConsumerTest {

    @Mock
    private WMSSenderService senderService;

    @InjectMocks
    private WMSXmlConsumer consumer;

    @Test
    void processWmsXml() {
        ConsumerRecord<String, String> consumerRecord = new ConsumerRecord<String, String>("wtm", 3, 3, "key", "xml");
        doNothing().when(senderService).sendXmlToWms(anyString());
        consumer.processWmsXml(consumerRecord, "xml");
        verify(senderService, times(1)).sendXmlToWms(anyString());
    }
}